package stef.todoapp;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class TodoFragment extends Fragment implements TodoAdapter.Listener {

    private RecyclerView recyclerView;
    private TodoAdapter adapter;
    private List<Todo> todoList;
    private ImageView mButtonAdd;
    private ImageView exit;
    private ImageView chart;
    private PopupWindow mPopupWindow;
    private FirebaseDatabase database;
    private FirebaseAuth mAuth;

    public TodoFragment() {
    }

    public static TodoFragment newInstance() {
        TodoFragment fragment = new TodoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = FirebaseDatabase.getInstance();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.todo_frag, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        View v = getView();
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        Toolbar toolbar = null;
        if (v != null) {
            toolbar = v.findViewById(R.id.toolbar);
            recyclerView = v.findViewById(R.id.recycler_view);
        }
        if (activity != null) {

            activity.setSupportActionBar(toolbar);
            ActionBar actionBar = activity.getSupportActionBar();
            actionBar.setTitle("Todo");
        }
        todoList = new ArrayList<>();
        adapter = new TodoAdapter(this.getContext(), todoList, this);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this.getContext(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
        mButtonAdd = view.findViewById(R.id.add);
        exit = view.findViewById(R.id.exit);
        chart = view.findViewById(R.id.chart);
        mAuth = FirebaseAuth.getInstance();
        chart.setOnClickListener(v1 -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.frame_container, ChartFragment.newInstance())
                    .commitNow();
        });
        mButtonAdd.setOnClickListener(v1 -> {
            showAddTodo();
        });
        exit.setOnClickListener(v1 -> {
            mAuth.signOut();
            Intent intent = new Intent(getContext(), Login.class);
            startActivity(intent);
            getActivity().getFragmentManager().popBackStack();
        });

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        loadData();

    }

    private void loadData() {
        List<Todo> todos = new ArrayList<>();
        DatabaseReference databaseReference = database.getReference();
        databaseReference.keepSynced(true);
        databaseReference.child("todo").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnap : dataSnapshot.getChildren()) {
                    Todo d = childSnap.getValue(Todo.class);
                    todos.add(d);
                }
                todoList.addAll(todos);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void showAddTodo() {
        // Initialize a new instance of LayoutInflater service
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);

        // Inflate the custom layout/view
        View customView = inflater.inflate(R.layout.edit_todo, null);
        ImageView closeButton = customView.findViewById(R.id.ib_close);
        EditText title = customView.findViewById(R.id.nameTodo);
        EditText desc = customView.findViewById(R.id.description);
        Button save = customView.findViewById(R.id.save);
        mPopupWindow = new PopupWindow(
                customView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                true
        );
        mPopupWindow.setBackgroundDrawable(new ColorDrawable());
        mPopupWindow.setOutsideTouchable(true);
        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }
        save.setOnClickListener(v -> {
            DatabaseReference database = FirebaseDatabase.getInstance().getReference();
            String id = database.child("todo").push().getKey();
            Todo todo = new Todo(id, title.getText().toString(), desc.getText().toString());
            database.child("todo").child(id).setValue(todo);
            todoList.add(todo);
            adapter.notifyDataSetChanged();
            mPopupWindow.dismiss();
        });

        // Get a reference for the custom view close button
        closeButton.setOnClickListener(view -> {
            // Dismiss the popup window
            mPopupWindow.dismiss();
        });
        mPopupWindow.showAtLocation(getView(), Gravity.CENTER, 0, 0);
    }

    @Override
    public void onSelectItem(final int position, String action) {
        final Todo d = todoList.get(position);
        if (action.equals("action_remove")) {
            DatabaseReference databaseReference = database.getReference().child("todo");
            databaseReference.keepSynced(true);
            databaseReference.child(d.getId()).removeValue();
            todoList.remove(position);
            adapter.notifyDataSetChanged();
            Toast.makeText(getContext(), "Deleted with success.", Toast.LENGTH_SHORT).show();
        } else if (action.equals("action_search")) {
            String escapedQuery = null;
            try {
                escapedQuery = URLEncoder.encode(d.getTitle(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            Uri uri = Uri.parse("http://www.google.com/#q=" + escapedQuery);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);

        } else if (action.equals("action_edit")) {
            showEditTodo(d, position);
        }

    }

    private void showEditTodo(Todo d, Integer position) {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);

        // Inflate the custom layout/view
        View customView = inflater.inflate(R.layout.edit_todo, null);
        ImageView closeButton = customView.findViewById(R.id.ib_close);
        EditText title = customView.findViewById(R.id.nameTodo);
        EditText desc = customView.findViewById(R.id.description);
        Button save = customView.findViewById(R.id.save);
        title.setText(d.getTitle());
        desc.setText(d.getDescription());
        mPopupWindow = new PopupWindow(
                customView,
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT,
                true
        );
        mPopupWindow.setBackgroundDrawable(new ColorDrawable());
        mPopupWindow.setOutsideTouchable(true);
        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            mPopupWindow.setElevation(5.0f);
        }
        save.setOnClickListener(v -> {
            DatabaseReference databaseReference = database.getReference();
            databaseReference.keepSynced(true);
            d.setTitle(title.getText().toString());
            d.setDescription(desc.getText().toString());
            databaseReference.child("todo").child(d.getId()).setValue(d);
            todoList.set(position, d);
            adapter.notifyItemChanged(position);
            mPopupWindow.dismiss();
            Toast.makeText(getContext(), "Success.", Toast.LENGTH_SHORT).show();
        });

        // Get a reference for the custom view close button
        closeButton.setOnClickListener(view -> {
            // Dismiss the popup window
            mPopupWindow.dismiss();
        });
        mPopupWindow.showAtLocation(getView(), Gravity.CENTER, 0, 0);

    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}
