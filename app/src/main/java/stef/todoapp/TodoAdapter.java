package stef.todoapp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.MyViewHolder> {

    private Context mContext;
    private List<Todo> todos;
    private Listener mListener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, description, id;
        ImageView overflow;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            description = view.findViewById(R.id.description);
            overflow = view.findViewById(R.id.overflow);
        }
    }

    public TodoAdapter(Context mContext, List<Todo> albumList, Listener listener) {
        this.mContext = mContext;
        this.todos = albumList;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.todo_card, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Todo todo = todos.get(position);
        holder.title.setText(todo.getTitle());
        holder.description.setText(todo.getDescription());
        holder.overflow.setOnClickListener(v -> showPopupMenu(holder.overflow, position));
    }

    private int showPopupMenu(View view, int pos) {
        PopupMenu popup = new PopupMenu(mContext, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new MyMenuItemClickListener(pos));
        popup.show();
        return pos;
    }

    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {

        int position;

        MyMenuItemClickListener(int position) {
            this.position = position;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.action_remove:
                    mListener.onSelectItem(position, "action_remove");
                    return true;
                case R.id.action_search:
                    mListener.onSelectItem(position, "action_search");
                    return true;
                case R.id.action_edit:
                    mListener.onSelectItem(position, "action_edit");
                    return true;
                default:
            }
            return false;
        }
    }

    @Override
    public int getItemCount() {
        return todos.size();
    }

    public interface Listener {
        void onSelectItem(int position, String action);
    }
}
